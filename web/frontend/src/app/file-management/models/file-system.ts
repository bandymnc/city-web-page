export class FileSystem{
    constructor(
        public directories: Array<string>,
        public files: Array<string>,
        public basePath: string
    ){}
}