export class DeleteFile {
    constructor(
        public path: string,
        public recursiveDelete: boolean
    ) { }
}