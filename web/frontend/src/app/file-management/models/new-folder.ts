export class NewFolder{
    constructor(
        public path: string,
        public newFolderName: string
    ){}
}