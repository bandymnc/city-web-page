export class UploadFiles{
    constructor(
        public files: FormData,
        public path: string
    ){}
}