import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FileSystem } from '../models/file-system';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { FileManagementService } from '../file-management.service';
import { DeleteFile } from '../models/delete-file';
import { ConfirmDialogComponent } from 'src/app/dialog/confirm-dialog/confirm-dialog.component';
import { FileSystemPath } from '../models/file-system-path';

@Component({
  selector: 'app-file-explorer',
  templateUrl: './file-explorer.component.html',
  styleUrls: ['./file-explorer.component.scss']
})
export class FileExplorerComponent implements OnInit {
  @Input() fileSystem: FileSystem;
  @Input() isDelete: boolean = false;
  @Output() path = new EventEmitter<string>();
  @Output() downloadFilePath = new EventEmitter<string>();

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private fileManagementService: FileManagementService,
  ) { }

  ngOnInit(): void {
  }

  updateFileSystem(directoryPath: string) {
    this.path.emit(directoryPath);
  }

  getFile(filePath: string) {
    this.downloadFilePath.emit(filePath);
  }

  deleteDirectory(path: string, isRecursive: boolean) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      disableClose: true,
      data: { msg: "Biztos törölni akarod a mappát?" }
    });

    dialogRef.afterClosed().subscribe(
      succeeded => {
        if (succeeded) {
          this.deleteDirectoryByService(path, isRecursive);
        }
      }
    );
  }

  deleteDirectoryByService(path: string, isRecursive: boolean) {
    this.fileManagementService.deleteDirectory(new DeleteFile(path, isRecursive))
      .subscribe(res => {
        if (res.succeeded) {
          this.toastr.success("Sikeres törlés!");
          let index = this.fileSystem.directories.indexOf(this.fileSystem.directories.find(f => f === path));
          this.fileSystem.directories.splice(index, 1);
        }
        if (res.error && res.error.code == "allowRecursiveDelete") {
          this.confirmRecursiveDirectoryDelete(path);
        }
      },
        err => {
          console.log(err);
        });
  }

  confirmRecursiveDirectoryDelete(path: string) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      disableClose: true,
      data: { msg: "A mappa nem üres, így is törölni szeretnéd?" }
    });

    dialogRef.afterClosed().subscribe(
      succeeded => {
        if (succeeded) {
          this.deleteDirectoryByService(path, true);
        }
      }
    );
  }

  deleteFile(path: string) {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      disableClose: true,
      data: { msg: 'Biztos törölni akarod a file-t?' }
    });

    dialogRef.afterClosed().subscribe(
      succeeded => {
        if (succeeded) {
          this.deleteFileByService(path);
        }
      }
    );
  }

  deleteFileByService(path) {
    this.fileManagementService.deleteFile(new FileSystemPath(path))
      .subscribe(res => {
        if (res.succeeded) {
          this.toastr.success("Sikeres törlés!");

          let index = this.fileSystem.files.indexOf(this.fileSystem.files.find(f => f === path));
          this.fileSystem.files.splice(index, 1);
        } else {
          this.toastr.warning("Törlés sikertelen!");
        }
      },
        err => {
          console.log(err);
        });
  }
}
