import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limitTo'
})
export class LimitToPipe {
  transform(value: string) : string {

    let limit = 10;
    let trail = '...';

    return value.length > limit ? value.substring(0, limit) + trail : value;
  }
}