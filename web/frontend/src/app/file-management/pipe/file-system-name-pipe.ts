import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'splitName'})
export class FileSystemNamePipe implements PipeTransform {
  transform(value: string): string {
    let nameTmb = value.split("\\");

    return nameTmb[nameTmb.length -1];
  }
}