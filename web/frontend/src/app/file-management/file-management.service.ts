import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FileSystemPath } from './models/file-system-path';
import { NewFolder } from './models/new-folder';
import { DeleteFile } from './models/delete-file';

@Injectable({
  providedIn: 'root'
})
export class FileManagementService {

  constructor(
    private http: HttpClient
  ) { }

  getFileSystem(path: FileSystemPath):Observable<any>{
    return this.http.post(environment.apiEndPoint + 'fileSystem/files',path);
  }

  getFile(path: FileSystemPath): Observable<any>{
    return this.http.post(environment.apiEndPoint + 'fileSystem/getFile',path);
  }

  saveNewFolder(newFolder: NewFolder): Observable<any>{
    return this.http.post(environment.apiEndPoint + 'fileSystem/createNewFolder',newFolder);
  }

  deleteFile(path: FileSystemPath): Observable<any>{
    return this.http.post(environment.apiEndPoint + 'fileSystem/deleteFile',path);
  }

  deleteDirectory(deleteFile: DeleteFile): Observable<any>{
    return this.http.post(environment.apiEndPoint + 'fileSystem/deleteDirectory',deleteFile);
  }

  uploadFile(formData): Observable<any>{
    return this.http.post(environment.apiEndPoint + 'fileSystem/upload',formData,{reportProgress: true, observe: 'events'});
  }
}
