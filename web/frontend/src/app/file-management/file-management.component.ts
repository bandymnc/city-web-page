import { Component, OnInit } from '@angular/core';
import { FileManagementService } from './file-management.service';
import { FileSystem } from './models/file-system';
import { FileSystemPath } from './models/file-system-path';
import { MatDialog } from '@angular/material/dialog';
import { NewFolderDialogComponent } from '../dialog/new-folder-dialog/new-folder-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { FileDownloader } from '../helper/file-downloader';

@Component({
  selector: 'app-file-management',
  templateUrl: './file-management.component.html',
  styleUrls: ['./file-management.component.scss']
})
export class FileManagementComponent implements OnInit {
  fileSystem: FileSystem;
  backStack = 1;
  isDelete = false;
  files;
  uploadText = "Feltöltés"

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private fileManagementService: FileManagementService,
  ) { }

  ngOnInit(): void {
    this.fileSystem = new FileSystem(null, null, null);
    this.getFileSystem("");
  }

  getFileStructure(directory) {
    this.backStack++;
    this.getFileSystem(directory);
  }

  getFileSystem(path: string) {
    this.isDelete = false;
    this.fileManagementService.getFileSystem(new FileSystemPath(path))
      .subscribe(
        res => {
          this.fileSystem = res.fileStructure;
        },
        err => {
          console.log(err);
        }
      );
  }

  getFile(path: string) {
    this.fileManagementService.getFile(new FileSystemPath(path))
      .subscribe(
        res => {
          if (res.file) {
            this.downloadFile(res.file)
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  downloadFile(file): void {
    FileDownloader.downloadFile(file);
  }

  openNewFolderDialog() {
    const dialogRef = this.dialog.open(NewFolderDialogComponent, {
      disableClose: true,
      data: this.fileSystem.basePath
    });

    dialogRef.afterClosed().subscribe(
      succeeded => {
        if (succeeded) {
          this.toastr.success("Sikeres mentés!");
          this.getFileSystem(this.fileSystem.basePath);
        }
      }
    );
  }

  back() {
    var previousPath = this.fileSystem.basePath.substr(0, this.fileSystem.basePath.lastIndexOf("\\"));
    this.backStack--;
    this.getFileSystem(previousPath);
  }

  onFileComplete(completed: boolean){
    if(completed){
      this.getFileSystem(this.fileSystem.basePath);
      this.toastr.success("Sikeres mentés!");
    }
  }
}
