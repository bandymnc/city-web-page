import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RegistrationComponent } from './account/registration/registration.component';
import { LoginComponent } from './account/login/login.component';
import { AccountModalComponent } from './account/account-modal/account-modal.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { RecaptchaModule } from 'ng-recaptcha';
import { HomeComponent } from './home/home.component';
import { AuthInterceptor } from './auth/auth-interceptor';
import { ContactsComponent } from './contacts/contacts.component';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { PostComponent } from './post/post/post.component';
import {EditPostComponent} from './post/edit-post/edit-post.component';
import { PostDetailsComponent } from './post/post-details/post-details.component';
import { PostListComponent } from './post/post-list/post-list.component';
import { FourZeroFourComponent } from './four-zero-four/four-zero-four.component';
import { ConfirmDialogComponent } from './dialog/confirm-dialog/confirm-dialog.component';
import { FileManagementComponent } from './file-management/file-management.component';
import { NewFolderDialogComponent } from './dialog/new-folder-dialog/new-folder-dialog.component';
import { FileSystemNamePipe } from './file-management/pipe/file-system-name-pipe';
import { FileExplorerComponent } from './file-management/file-explorer/file-explorer.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { LimitToPipe } from './file-management/pipe/limit-to-pipe';
import { ForgottenPasswordComponent } from './account/forgotten-password/forgotten-password.component';
import { ChangePasswordComponent } from './account/change-password/change-password.component';
import { UserManagementComponent } from './user-management/user-management.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginComponent,
    AccountModalComponent,
    HomeComponent,
    ContactsComponent,
    EditPostComponent,
    PostComponent,
    PostDetailsComponent,
    PostListComponent,
    FourZeroFourComponent,
    ConfirmDialogComponent,
    FileManagementComponent,
    NewFolderDialogComponent,
    FileSystemNamePipe,
    LimitToPipe,
    FileExplorerComponent,
    FileUploadComponent,
    ForgottenPasswordComponent,
    ChangePasswordComponent,
    UserManagementComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    MaterialFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(
      {
        timeOut: 2000,
        positionClass: 'toast-top-right',
        preventDuplicates: true,
        closeButton: true
      }),
    RecaptchaModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }],

  bootstrap: [AppComponent]
})
export class AppModule { }
