import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactsComponent } from './contacts/contacts.component';
import { SuperUserGuard } from './auth/user-level-guards/super-user.guard';
import { PostComponent } from './post/post/post.component';
import { PostDetailsComponent } from './post/post-details/post-details.component';
import { PostListComponent } from './post/post-list/post-list.component';
import { FourZeroFourComponent } from './four-zero-four/four-zero-four.component';
import { EditPostComponent } from './post/edit-post/edit-post.component';
import { FileManagementComponent } from './file-management/file-management.component';
import { ChangePasswordComponent } from './account/change-password/change-password.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { AdminGuard } from './auth/user-level-guards/admin.guard';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'changePassword/:id', component: ChangePasswordComponent},
  {
    path: 'post', component: PostComponent, children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'edit', component: EditPostComponent, canActivate: [SuperUserGuard] },
      { path: 'edit/:id', component: EditPostComponent ,canActivate: [SuperUserGuard] },
      { path: 'details/:id', component: PostDetailsComponent },
      { path: 'list', component: PostListComponent},
    ]
  },
  { path: 'repository', component: FileManagementComponent },
  { path: 'users',component: UserManagementComponent,canActivate: [AdminGuard]},
  { path: '**', component: FourZeroFourComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
