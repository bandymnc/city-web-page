import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, NgForm, FormControl, Validators } from '@angular/forms';
import { FileManagementService } from 'src/app/file-management/file-management.service';
import { NewFolder } from 'src/app/file-management/models/new-folder';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-new-folder-dialog',
  templateUrl: './new-folder-dialog.component.html',
  styleUrls: ['./new-folder-dialog.component.scss']
})
export class NewFolderDialogComponent implements OnInit {
  newFolderName = new FormControl(null, Validators.required);

  constructor(
    @Inject(MAT_DIALOG_DATA) public path: string,
    private dialogRef: MatDialogRef<NewFolderDialogComponent>,
    private fileManagementService: FileManagementService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  saveNewFolder() {
    var newFolder = new NewFolder(this.path, this.newFolderName.value);
    this.fileManagementService.saveNewFolder(newFolder)
      .subscribe(
        res => {
          if (res.succeeded) {
            this.dialogRef.close(true)
          }

          if (res.error && res.error.code == "duplicatedDirectoryName") {
            this.toastr.error("Egyedi mappanevet adjon meg!");
          }
        },
        err => {
          console.log(err);
        }
      );
  }

}
