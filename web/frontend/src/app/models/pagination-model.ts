export class PaginationModel {

    constructor(
        public pageSize: number,
        public pageIndex: number,
        public searchString: string,
        public orderBy: string,
        public orderByName?: string
    ) { }
}