import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../models/post';
import { JwtTokenHandler } from 'src/app/helper/jwt-token-handler';
import { PostService } from '../post.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {
  @Input() removableInput: string
  post: Post;
  postId: string = null
  formModel: FormGroup;
  img: string;
  isPostPicture = "true";
  param = "file";

  constructor(
    private jwtHandler: JwtTokenHandler,
    private postService: PostService,
    private router: Router,
    private toastr: ToastrService,
    private builder: FormBuilder,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.postId = this.route.snapshot.paramMap.get("id");

    if (this.postId != null) {
      this.getPost();
    }
    this.formModel = this.builder.group({
      title: new FormControl(null),
      body: new FormControl(null),
      picture: new FormControl(null)
    });
  }

  setUpImage(imageUrl: string){
    this.img = imageUrl;
  }

  onFileComplete(completed: boolean){
    if(completed){
      this.toastr.success("Sikeres képfeltöltés!");
    }
  }

  onSubmit() {
    this.post = new Post(
      null,
      this.formModel.get("title").value,
      this.formModel.get("body").value,
      this.jwtHandler.getUserId(),
      this.img,
      null
    )
    this.postService.newPost(this.post)
      .subscribe(
        res => {
          this.toastr.success("Sikeres mentés!");
          this.router.navigate(["/post"]);
        },
        err => {
          this.toastr.error("Valami hiba történt");
        }
      );
  }

  getPost() {
    this.postService.getPostById(+this.postId)
      .subscribe(
        res => {
          this.post = res.post;
          this.formModel.controls['title'].setValue(this.post.title);
          this.formModel.controls['body'].setValue(this.post.body);
          this.formModel.controls['picture'].setValue(this.post.picture);
          this.img = this.post.picture;
        },
        err => {
          console.log(err);
        }
      );
  }

  deletePicture() {
    let filePath = {
      "filePath": this.img
    }

    this.postService.deletePicture(filePath)
      .subscribe(
        res => {
          if(res.success || res.error.code === "pictureAlreadyUsed"){
            this.img = null;
            this.toastr.info("Sikeres törlés!");
            this.formModel.get("picture").reset();
            //this.updatePost()
          }else{
            this.toastr.warning(res.error.description);
          }
          
        },
      );
  }

  updatePost() {
    this.post.title = this.formModel.get("title").value;
    this.post.body = this.formModel.get("body").value;
    this.post.userId = this.jwtHandler.getUserId();
    this.post.picture = this.img;

    this.postService.updatePost(this.postId,this.post)
      .subscribe(
        res => {
          if (res.succeeded) {
            this.toastr.success("Sikeres módosítás!");
            this.router.navigateByUrl('/post/list');
          }
        }
      )
  }

  get formControls() { return this.formModel.controls; }
}
