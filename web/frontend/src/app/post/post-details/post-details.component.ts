import { Component, OnInit } from '@angular/core';
import { PostService } from '../post.service';
import { ActivatedRoute } from '@angular/router';
import { Post } from '../models/post';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit {
  id: number;
  post:Post;

  constructor(
    private postServive: PostService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.post = new Post(null,"","","","","");
    this.route.params.subscribe(params => {
      console.log(params['id']);
      this.getPost(params['id']);
    });
  }

  getPost(id){
    this.postServive.getPostById(id)
      .subscribe(
        res =>{
          this.post = res.post;
          console.log(this.post);
        },
        err => {
          console.log(err);
        }
      );
  }

}
