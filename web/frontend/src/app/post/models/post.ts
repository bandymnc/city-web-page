export class Post {
    constructor(
        public id: number,
        public title: string,
        public body: string,
        public userId: string,
        public picture: string,
        public userName: string,
        public views?:number,
        public lastUpdate?: string
    ) { }
}