import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Post } from './models/post';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PaginationModel } from '../models/pagination-model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http:HttpClient) { }

  newPost(post: Post):Observable<any>{
    return this.http.post(environment.apiEndPoint + 'post/new',post);
  }

  deletePicture(filePath):Observable<any>{
    return this.http.post(environment.apiEndPoint + "FileSystem/deletePicture",filePath);
  }

  getAllPostWithPagination(pagination: PaginationModel): Observable<any> {
    return this.http.post(environment.apiEndPoint + 'post/getAllWithPaginator',pagination);
  }

  getPostById(id: number):Observable<any>{
    return this.http.get(environment.apiEndPoint + 'post/get/' + id);
  }

  deletePost(id: number): Observable<any>{
    return this.http.delete(environment.apiEndPoint + 'post/delete/' + id);
  }

  updatePost(postId:string,post: Post): Observable<any>{
    return this.http.post(environment.apiEndPoint + 'post/update/'+postId,post);
  }
}
