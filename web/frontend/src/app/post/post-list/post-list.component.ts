import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { JwtTokenHandler } from 'src/app/helper/jwt-token-handler';
import { PostService } from '../post.service';
import { Post } from '../models/post';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/dialog/confirm-dialog/confirm-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { PaginationModel } from 'src/app/models/pagination-model';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  length: number;
  isSuperUser: boolean;
  postWithPagination: PaginationModel
  posts: Post[];
  pageEvent: PageEvent;
  constructor(
    private jwtHandler: JwtTokenHandler,
    private postService: PostService,
    private router: Router,
    private dialog: MatDialog,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.isSuperUser = this.jwtHandler.isSuperUser();
    this.postWithPagination = new PaginationModel(5, 0, "", "dec");

    this.getAllPost();
  }

  paginatorEvent(data) {
    this.postWithPagination.pageIndex = data.pageIndex;
    this.postWithPagination.pageSize = data.pageSize;

    this.getAllPost();
  }

  getAllPost() {
    this.postService.getAllPostWithPagination(this.postWithPagination)
      .subscribe(
        res => {
          this.posts = res.posts;
          this.length = res.length;
        },
        err => {
          console.log(err);
        }
      );
  }

  updatePost(id: number) {
    this.router.navigateByUrl("/post/edit/" + id);
  }

  deletePost(id: number) {
    this.postService.deletePost(id)
      .subscribe(
        res => {
          if (res) {
            this.toastr.success("Sikeres törlés!")
            let index = this.posts.indexOf(this.posts.find(p => p.id === id));
            this.posts.splice(index,1);
          }
        }
      );
  }

  openDeleteConfirmDialog(id: number){
    const dialogRef = this.dialog.open(ConfirmDialogComponent,{
      data:{
        msg: 'Biztos törölni akarod?'}
    });

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.deletePost(id);
      }
    });
  }
}
