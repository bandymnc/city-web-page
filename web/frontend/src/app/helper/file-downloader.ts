import { FileEncode } from './file-encode';

export class FileDownloader{

    public static downloadFile(file){
        const blob: Blob = FileEncode.base64ToBlob(file.file, file.fileType)
        const fileName: string = file.fileName;
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
    
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();
    
        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }
}