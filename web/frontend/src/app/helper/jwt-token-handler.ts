import { Injectable } from '@angular/core';
import * as jwt_decode from "jwt-decode";
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})

export class JwtTokenHandler {

    getUserLevel(): any {
        let tokenInfo = this.getDecodedAccessToken();
        
        if(tokenInfo != null){
            return tokenInfo.role;
        }

        return null;
    }

    isSuperUser(){
        let tokenInfo = this.getDecodedAccessToken();
        
        if(tokenInfo != null && tokenInfo.role == "Super User"){
            return true;
        }

        return false;
    }

    isAdmin(){
        let tokenInfo = this.getDecodedAccessToken();
        
        if(tokenInfo != null && tokenInfo.role == "Admin"){
            return true;
        }

        return false;
    }

    isLoggedIn(): boolean {
        return localStorage.getItem(environment.accesTokenName) != null;
    }

    getUserId(){
        let tokenInfo = this.getDecodedAccessToken();
        
        if(tokenInfo != null){
            return tokenInfo.UserID;
        }

        return null;
    }

    getDecodedAccessToken(): any {
        try{
            return jwt_decode(localStorage.getItem(environment.accesTokenName));
        }
        catch(Error){
            return null;
        }
      }

}