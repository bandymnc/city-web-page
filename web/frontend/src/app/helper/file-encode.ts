export class FileEncode{

    public static base64ToBlob(b64Data: string, fileType: string, sliceSize=512) {
        b64Data = b64Data.replace(/\s/g, ''); //IE compatibility...
        let byteCharacters = atob(b64Data);
        let byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            let slice = byteCharacters.slice(offset, offset + sliceSize);
    
            let byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            let byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        
        return new Blob(byteArrays, {type: this.selectContentType(fileType)});
    }

    private static selectContentType(fileType: string):string {
        switch (fileType) {
            case "pdf":
                return "application/pdf";
            case "doc":
                return "application/msword"
            case "png":
                return "application/png";
            case "gif":
                return "image/gif";
            case "pdf":
                return "application/pdf";
            case "jpeg":
                return "image/jpeg";
            case "jpg":
                return "image/jpeg";
            case "ppt":
                return "application/vnd.ms-powerpoint";
            case "docx":   
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            default:
                return null;
        }
      }
}