import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpEventType } from '@angular/common/http';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { FileManagementService } from '../file-management/file-management.service';
import { FileUploadModel } from './FileUploadModel';

@Component({
      selector: 'app-file-upload',
      templateUrl: './file-upload.component.html',
      styleUrls: ['./file-upload.component.scss'],
      animations: [
            trigger('fadeInOut', [
                  state('in', style({ opacity: 100 })),
                  transition('* => void', [
                        animate(300, style({ opacity: 0 }))
                  ])
            ])
      ]
})
export class FileUploadComponent implements OnInit {
      
      @Input() isPostPicture = "false";
      /** Link text */
      @Input() text = 'Upload';
      /** Name used in form which will be sent in HTTP request. */
      @Input() param = 'file';
      /** Target URL for file uploading. */
      @Input() basePath = "";
      /** Allow you to add handler after its completion. Bubble up response text from remote. */
      @Output() complete = new EventEmitter<boolean>();
      /** If picture was uploaded, then the API response will be an url string */
      @Output() pictureUrl = new EventEmitter<string>();

      public files: Array<FileUploadModel> = [];

      constructor(
            private fileUploadService: FileManagementService) { }

      ngOnInit() {
      }

      onClick() {
            const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
            fileUpload.onchange = () => {
                  for (let index = 0; index < fileUpload.files.length; index++) {
                        const file = fileUpload.files[index];
                        this.files.push({
                              data: file,
                              inProgress: false, progress: 0, canRetry: false, canCancel: true
                        });
                  }
                  this.uploadFiles();
            };
            fileUpload.click();
      }

      cancelFile(file: FileUploadModel) {
            file.sub.unsubscribe();
            this.removeFileFromArray(file);
      }

      retryFile(file: FileUploadModel) {
            this.uploadFile(file);
            file.canRetry = false;
      }

      private uploadFile(file: FileUploadModel) {
            const fd = new FormData();
            fd.append(this.param, file.data);
            fd.append("path", this.basePath);
            fd.append("isProfilePicture",this.isPostPicture);

            file.inProgress = true;
            file.sub = this.fileUploadService.uploadFile(fd)
                  .subscribe(event => {
                        if (event.type === HttpEventType.UploadProgress) {
                              file.progress = Math.round(event.loaded * 100 / event.total);
                        }
                        if (event.type === HttpEventType.Response) {
                              if (event.body.succeeded) {
                                    this.removeFileFromArray(file);
                                    this.complete.emit(true);
                              }
                              if (event.body.path) {
                                    this.pictureUrl.emit(event.body.path);
                              }
                              if (event.body.error) {
                                    file.inProgress = false;
                                    file.canRetry = true;
                              }
                        }
                  });
      }

      private uploadFiles() {
            const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
            fileUpload.value = '';

            this.files.forEach(file => {
                  this.uploadFile(file);
            });
      }

      private removeFileFromArray(file: FileUploadModel) {
            const index = this.files.indexOf(file);
            if (index > -1) {
                  this.files.splice(index, 1);
            }
      }
}
