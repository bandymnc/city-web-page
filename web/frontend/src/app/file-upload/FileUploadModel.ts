import { Subscription } from 'rxjs';

export class FileUploadModel {
    data: File;
    inProgress: boolean;
    progress: number;
    canRetry: boolean;
    canCancel: boolean;
    sub?: Subscription;
}