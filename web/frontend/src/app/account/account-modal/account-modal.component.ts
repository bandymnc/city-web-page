import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatButton } from '@angular/material/button';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-account-modal',
  templateUrl: './account-modal.component.html',
  styleUrls: ['./account-modal.component.scss']
})
export class AccountModalComponent implements OnInit {
  @ViewChild('loginButton') loginButton: MatButton;
  @ViewChild('registerButton') registerButton: MatButton;
  isLoginActive: boolean;

  constructor(
    private dialogRef: MatDialogRef<AccountModalComponent>,
    private account: AccountService) { }

  ngOnInit(): void {
    this.isLoginActive = true;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  activate() {
  }

}