export class ChangePasswordModel{
    constructor(
        public forgotPasswordToken: string,
        public newPassword: string
    ){}
}