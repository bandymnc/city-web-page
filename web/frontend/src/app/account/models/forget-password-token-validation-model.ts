export class ForgetPasswordTokenValidationModel{
    constructor(
        public forgotPasswordGeneratedToken: string
    ){}
}