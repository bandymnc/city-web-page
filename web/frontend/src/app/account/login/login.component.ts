import { Component, OnInit, Input } from '@angular/core';
import { Login } from '../models/login';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AccountService } from '../account.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef } from '@angular/material/dialog';
import { AccountModalComponent } from '../account-modal/account-modal.component';
import { Recaptcha } from '../models/recaptcha';
import { containDigit } from '../validators/password-digit-validator';
import { containUpperCase } from '../validators/password-uppercase-validator'
import { environment } from 'src/environments/environment';
import { UserEmailModel } from '../models/user-email-model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Input() count: number;
  loginModel: Login;
  formModel: FormGroup;
  isRecaptchaSucceeded: boolean;
  inProgress = false;
  forgottenPassword = false;
  forgottenPasswordForm: FormGroup;

  constructor(
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private dialogRef: MatDialogRef<AccountModalComponent>) { }

  ngOnInit() {
    //False-ra kell állítani
    this.isRecaptchaSucceeded = true;

    this.formModel = this.formBuilder.group({
      email: new FormControl(null, {
        validators: [Validators.required, Validators.email],
        updateOn: "blur"
      }),
      password: new FormControl(null, {
        validators: [Validators.required, containUpperCase, containDigit],
        updateOn: "blur"
      }),
    }, { updateOn: "submit" })

    this.forgottenPasswordForm = this.formBuilder.group({
      email: new FormControl(null, [Validators.email, Validators.required])
    });
  }

  onSubmit() {
    if (!this.formModel.invalid && this.isRecaptchaSucceeded) {
      this.loginModel = new Login();
      this.loginModel.email = this.formModel.controls['email'].value;
      this.loginModel.password = this.formModel.controls['password'].value;
      this.inProgress = true;

      this.accountService.login(this.loginModel)
        .subscribe(
          res => {
            if (res.error != null) {
              this.toastr.error(res.error.description);
              this.inProgress = false;
            } else {
              this.storeToken(res.tokens);
              this.toastr.success("Sikeres bejelentkezés!");
              this.dialogRef.close(true);
            }
          },
          err => {
            console.log(err);
          }
        );
    }
  }

  private storeToken(tokens) {
    localStorage.setItem(environment.accesTokenName, tokens.accessToken);
    localStorage.setItem(environment.refreshTokenName, tokens.refreshToken);
  }

  resolved(captchaResponse: string) {
    let recaptcha = new Recaptcha();
    recaptcha.token = captchaResponse;

    this.accountService.validateCaptcha(recaptcha)
      .subscribe(
        res => {
          if (res.success) {
            this.isRecaptchaSucceeded = true;
          } else {
            this.toastr.error("Valami hiba történt, próbálja meg később.");
            grecaptcha.reset();
          }
        }
      );
  }

  sendForgottenPasswordEmail(){
    this.inProgress = true;
    let userEmailModel = new UserEmailModel(this.forgottenPasswordForm.controls['email'].value);
    this.accountService.sendForgottenPasswordEmail(userEmailModel)
      .subscribe(
        res=> {
          if(res.succeeded){
            this.forgottenPassword = false;
            this.inProgress = false;
            this.toastr.success("A szükséges adatokat elküldtük a megadott email címre!");
          }
          if(res.error && res.error.code == "emailNotFound"){
            this.toastr.error("A megadott email címmel nem található felhasználó!");
            this.inProgress = false;
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  get formControls() { return this.formModel.controls; }

  get ForgottenPasswordFormControl() { return this.forgottenPasswordForm.controls }
}
