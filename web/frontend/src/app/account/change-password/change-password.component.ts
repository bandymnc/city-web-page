import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { containDigit } from '../validators/password-digit-validator';
import { containUpperCase } from '../validators/password-uppercase-validator';
import { MustMatch } from '../validators/input-match-validator';
import { AccountService } from '../account.service';
import { ChangePasswordModel } from '../models/change-password-model';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { ForgetPasswordTokenValidationModel } from '../models/forget-password-token-validation-model';
import { Recaptcha } from '../models/recaptcha';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  formModel: FormGroup;
  inProgress = false;
  forgotPasswordToken: string;
  isKeyCorrect = false;
  isRecaptchaSucceeded: boolean;

  constructor(
    private builder: FormBuilder,
    private accountService: AccountService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.forgotPasswordToken = this.route.snapshot.paramMap.get('id');
    this.checkKey();

    this.formModel = this.builder.group({
      passwords: this.builder.group({
        password: new FormControl(null, {
          validators: [Validators.required, Validators.minLength(10), containDigit, containUpperCase],
          updateOn: 'blur'
        }),
        confirmPassword: new FormControl(null, {
          validators: [Validators.required],
          updateOn: 'blur'
        })
      }, { validators: MustMatch('password', 'confirmPassword'), })
    });
  }

  resolved(captchaResponse: string) {
    let recaptcha = new Recaptcha();
    recaptcha.token = captchaResponse;

    this.accountService.validateCaptcha(recaptcha)
      .subscribe(
        res => {
          if (res.success) {
            this.isRecaptchaSucceeded = true;
          } else {
            this.toastr.error("Valami hiba történt, próbálja meg később.");
            grecaptcha.reset();
          }
        }
      );
  }

  checkKey(){
    let forgetPasswordTokenValidationModel = new ForgetPasswordTokenValidationModel(this.forgotPasswordToken);

    this.accountService.checkKeyValidation(forgetPasswordTokenValidationModel)
      .subscribe(
        res=> {
          if(res.succeeded){
            this.isKeyCorrect = true;
          }
          else if(res.error?.code == "tokenNotFound"){
            this.toastr.warning("A megadott kód nem található!");
          }
          else if(res.error?.code == "tokenNotValid"){
            this.toastr.warning("Az egyedi azonosító lejárt, kérjen újat!");
          }
        },
        err =>{
          this.toastr.error("Valamilyen hiba történt, próbálja később.");
        }
      );
  }

  onSubmit(){
    let changePasswordModel = new ChangePasswordModel(
      this.forgotPasswordToken,
      this.formModel.controls['passwords'].get('password').value
    );

    this.accountService.changePassword(changePasswordModel)
      .subscribe(
        res=> {
          if(res.succeeded){
            this.toastr.success("Sikeres jelszó módosítás!");
          }
        },
        err => {
          console.log(err);
          this.toastr.error("Valamilyen hiba történt, próbálja újra");
        }
      );
  }

  get formControls() { return this.formModel.controls; }
  get password() { return this.formModel.controls['passwords'].get('password') }
  get confirmPassword() { return this.formModel.controls['passwords'].get('confirmPassword') }

}
