import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AccountService } from '../account.service';
import { ToastrService } from 'ngx-toastr';
import { Registration } from '../models/registration';
import { MustMatch } from '../validators/input-match-validator';
import { containDigit } from '../validators/password-digit-validator';
import { containUpperCase } from '../validators/password-uppercase-validator'
import { environment } from 'src/environments/environment';
import { Login } from '../models/login';
import { MatDialogRef } from '@angular/material/dialog';
import { AccountModalComponent } from '../account-modal/account-modal.component';
import { Recaptcha } from '../models/recaptcha';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  registrationModel: Registration;
  loginModel: Login;
  formModel: FormGroup;
  minLength: number;
  isRecaptchaSucceeded: boolean;
  inProgress = false;

  constructor(
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private dialogRef: MatDialogRef<AccountModalComponent>) { }

  ngOnInit(): void {
    this.minLength = environment.passwordMinLength;
    //False-ra kell állítani
    this.isRecaptchaSucceeded = true;

    this.formModel = this.formBuilder.group({
      firstName: new FormControl(null, {
        validators: [Validators.required],
        updateOn: 'blur'
      }),
      lastName: new FormControl(null, {
        validators: [Validators.required,],
        updateOn: 'blur'
      }),
      userName: new FormControl(null, {
        validators: [Validators.required,],
        updateOn: 'blur'
      }),
      emails: this.formBuilder.group({
        email: new FormControl(null, {
          validators: [Validators.required, Validators.email],
          updateOn: "blur"
        }),
        confirmEmail: new FormControl(null, {
          validators: [Validators.required, Validators.email],
          updateOn: 'blur'
        })
      }, { validators: MustMatch('email', 'confirmEmail') }),
      passwords: this.formBuilder.group({
        password: new FormControl(null, {
          validators: [Validators.required, Validators.minLength(10), containDigit, containUpperCase],
          updateOn: 'blur'
        }),
        confirmPassword: new FormControl(null, {
          validators: [Validators.required],
          updateOn: 'blur'
        })
      }, { validators: MustMatch('password', 'confirmPassword'), })
    }, { updateOn: "submit" });
  }

  onSubmit() {

    if (!this.formModel.invalid && this.isRecaptchaSucceeded) {
      this.inProgress = true;
      this.registrationModel = new Registration();
      this.registrationModel.email = this.formModel.value.emails.email;
      this.registrationModel.password = this.formModel.value.passwords.password;
      this.registrationModel.firstName = this.formModel.value.firstName;
      this.registrationModel.lastName = this.formModel.value.lastName;
      this.registrationModel.userName = this.formModel.value.userName;

      this.accountService.register(this.registrationModel)
        .subscribe(
          res => {
            console.log(res);
            if (res.error != null) {
              this.toastr.error(res.error.description);
              this.inProgress = false;
            } else {
              this.storeToken(res.tokens);
              this.toastr.success("Sikeres regisztráció!");
              this.dialogRef.close(true);
            }
          },
          err => {
            console.log(err);
          }
        );
    }
  }

  private storeToken(tokens) {
    localStorage.setItem(environment.accesTokenName, tokens.accessToken);
    localStorage.setItem(environment.refreshTokenName, tokens.refreshToken);
  }

  resolved(captchaResponse: string) {
    let recaptcha = new Recaptcha();
    recaptcha.token = captchaResponse;

    this.accountService.validateCaptcha(recaptcha)
      .subscribe(
        res => {
          if (res.success) {
            this.isRecaptchaSucceeded = true;
          } else {
            this.toastr.error("Valami hiba történt, próbálja meg később.");
            grecaptcha.reset();
          }
        }
      );
  }

  get formControls() { return this.formModel.controls; }
  get email() { return this.formModel.controls['emails'].get('email') }
  get confirmEmail() { return this.formModel.controls['emails'].get('confirmEmail') }
  get password() { return this.formModel.controls['passwords'].get('password') }
  get confirmPassword() { return this.formModel.controls['passwords'].get('confirmPassword') }
}
