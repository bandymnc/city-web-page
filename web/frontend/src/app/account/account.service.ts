import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Login } from './models/login';
import { environment } from 'src/environments/environment';
import { Registration } from './models/registration';
import { Recaptcha } from './models/recaptcha';
import { ChangePasswordModel } from './models/change-password-model';
import { ForgetPasswordTokenValidationModel } from './models/forget-password-token-validation-model';
import { UserEmailModel } from './models/user-email-model';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http:HttpClient) { }

  login(loginModel: Login):Observable<any>{
    return this.http.post(environment.apiEndPoint + 'account/login',loginModel);
  }

  register(registrationModel: Registration):Observable<any>{
    return this.http.post(environment.apiEndPoint + 'account/registration',registrationModel);
  }

  validateCaptcha(recaptcha: Recaptcha): Observable<any>{
    return this.http.post(environment.apiEndPoint + 'account/VerifyCaptcha',recaptcha);
  }

  sendForgottenPasswordEmail(userEmailModel: UserEmailModel) : Observable<any>{
    return this.http.post(environment.apiEndPoint + 'account/sendForgotPasswordEmail',userEmailModel);
  }

  changePassword(changePasswordModel: ChangePasswordModel): Observable<any> {
    return this.http.post(environment.apiEndPoint + 'account/saveNewPassword',changePasswordModel);
  }

  checkKeyValidation(forgetPasswordTokenValidationModel: ForgetPasswordTokenValidationModel): Observable<any>{
    return this.http.post(environment.apiEndPoint + 'account/checkForgettenPasswordTokenValidation', forgetPasswordTokenValidationModel);
  }
}
