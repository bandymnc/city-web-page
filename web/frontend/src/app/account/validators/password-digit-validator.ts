import { AbstractControl } from '@angular/forms';
import { environment } from 'src/environments/environment';

export function containDigit(control: AbstractControl): { [key: string]: boolean } | null {

    if (control.value == null || control.value.length === 0) {
        return null;
    }

    if (!/[0-9]/.test(control.value) && environment.passwordContainDigit) {
        return { 'containDigit': true }
    }

    return null;
}