import { AbstractControl } from '@angular/forms';
import { environment } from 'src/environments/environment';

export function containUpperCase(control: AbstractControl): { [key: string]: boolean } | null {

    if (control.value == null || control.value.length === 0) {
        return null;
    }

    if (!/[A-Z]/.test(control.value) && environment.passwordContainUpperCase) {
        return { 'containUpperCase': true }
    }

    return null;
}