import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserModel } from './models/user-model';
import { UserRoleUpdateModel } from './models/user-role-update.model';
import { PaginationModel } from '../models/pagination-model';

@Injectable({
  providedIn: 'root'
})
export class UserManagementService {

  constructor(private readonly http: HttpClient) { }

  getUsersWithPagination(pagination: PaginationModel): Observable<any>{
    return this.http.post(environment.apiEndPoint + 'administration/users',pagination);
  }

  getRoles(): Observable<any>{
    return this.http.get(environment.apiEndPoint + 'administration/roles');
  }

  updateRole(userRoleUpdate: UserRoleUpdateModel): Observable<any>{
    return this.http.post(environment.apiEndPoint + 'administration/updateRole',userRoleUpdate);
  }
  
}
