import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserManagementService } from './user-management.service';
import { UserModel } from './models/user-model';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { UserRoleUpdateModel } from './models/user-role-update.model';
import { ToastrService } from 'ngx-toastr';
import { PaginationModel } from '../models/pagination-model';
import { MatPaginator } from '@angular/material/paginator';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  subject: Subject<string> = new Subject();
  users: UserModel[];
  displayedColumns: string[] = ['userId', 'fullName', 'email', 'role', 'updateRole'];
  dataSource;
  userRoleInUpdate = false;
  updatedUser: UserModel;
  roles;
  length: number;
  pagination: PaginationModel;
  selectedRole;

  constructor(
    private readonly userManagementService: UserManagementService,
    private readonly toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.pagination = new PaginationModel(5, 0, "", "desc","fullName");
    this.getUsers();
    this.getRoles();
    this.length = 0;


    this.subject.pipe(
      debounceTime(500))
      .subscribe(searchTextValue => {
        this.pagination.searchString = searchTextValue;
        this.getUsers();
      });
  }

  onKeyUp(searchTextValue: string){
    this.subject.next(searchTextValue);
  }

  getRoles() {
    this.userManagementService.getRoles()
      .subscribe(
        res => {
          this.roles = res.roles;
        }
      )
  }

  getUsers() {
    this.userManagementService.getUsersWithPagination(this.pagination)
      .subscribe(
        res => {
          if (res.users) {
            this.users = res.users;
            this.dataSource = new MatTableDataSource(this.users);
            this.length = res.count;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
          }
        }
      )
  }

  sortData(event){
    this.pagination.orderBy = event.direction;
    this.pagination.orderByName = event.active;

    if(this.pagination.orderBy == "") {
      this.pagination.orderBy = "desc"
    }
    
    this.getUsers();
  }

  updateUserRole(role) {
    let userRoleUpdateModel = new UserRoleUpdateModel(this.updatedUser.userId, role);

    this.userManagementService.updateRole(userRoleUpdateModel)
      .subscribe(
        res => {
          if (res.succeeded) {
            this.getUsers();
            this.toastr.success("Sikeres módosítás!");
            this.userRoleInUpdate = false;
          } else if (res.error) {
            this.toastr.warning("Módosítás nem lehetséges!");
          }
        }
      );
  }

  loadUserData(user) {
    this.userRoleInUpdate = true;
    this.updatedUser = user;
    this.selectedRole = user.role;
  }

  paginatorEvent(data) {
    this.pagination.pageIndex = data.pageIndex;
    this.pagination.pageSize = data.pageSize;
    this.getUsers();
  }
}
