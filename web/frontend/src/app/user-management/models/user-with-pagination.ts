import { UserModel } from './user-model';

export class UserWithPagination{
    constructor(
        public users: UserModel[],
        public count: number,
        public pageSize: number,
        public pageIndex: number,
        public searchString: string,
        public orderBy: string
    ){}
}