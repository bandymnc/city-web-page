export class UserRoleUpdateModel{
    constructor(
        public userId: string,
        public role: string
    ){}
}