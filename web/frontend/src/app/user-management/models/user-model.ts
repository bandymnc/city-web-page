export class UserModel{
    constructor(
        public userId: string,
        public role: string,
        public email: string,
        public fullName: string
    ){}
}