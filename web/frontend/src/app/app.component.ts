import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AccountModalComponent } from './account/account-modal/account-modal.component';
import { JwtTokenHandler } from './helper/jwt-token-handler';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isLoggedIn: boolean;
  isAdmin: boolean;

  constructor(
    private dialog: MatDialog,
    private jwtTokenHandler: JwtTokenHandler,
    private toastr: ToastrService) {}

  ngOnInit(): void {
    this.isLoggedIn = this.jwtTokenHandler.isLoggedIn();
    this.isAdmin = this.jwtTokenHandler.isAdmin();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AccountModalComponent, {
      panelClass: 'myapp-no-padding-dialog',
      maxHeight: window.innerHeight + 'px',
      disableClose: true 
    });

    dialogRef.afterClosed().subscribe(isResultFromLogin => {
      if(isResultFromLogin != null){
        window.location.reload();
      }
    });
  }

  logOut(){
    localStorage.clear();
    this.isLoggedIn = false;
    this.toastr.success("Sikeresen kijelentkezett!");
    window.location.reload();
  }
}
