import { Component, OnInit } from '@angular/core';
import { JwtTokenHandler } from '../helper/jwt-token-handler';
import { FileManagementService } from '../file-management/file-management.service';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
  picture: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  list = [];

  tiles: Tile[] = [
    {text: 'One', cols: 3, rows: 1, color: 'lightblue',picture: 'https://www.bacskiskun.hu/img/embeddable/kerekegyh-za-f-k-p.jpg'},
    {text: 'Two', cols: 1, rows: 2, color: 'lightgreen',picture: 'https://scontent.fbud4-1.fna.fbcdn.net/v/t31.0-8/458961_236836739760218_1740500808_o.jpg?_nc_cat=105&_nc_sid=a61e81&_nc_ohc=mCxL--WMCBQAX9pNJ18&_nc_ht=scontent.fbud4-1.fna&oh=b78c1a9cbe64f1224a7d610330d5b42b&oe=5EDC5EE6'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink',picture: 'https://scontent.fbud4-1.fna.fbcdn.net/v/t1.0-9/80085984_2615682245189354_2292638231046389760_o.jpg?_nc_cat=109&_nc_sid=8024bb&_nc_ohc=hNW79zs2oTkAX8YxK9H&_nc_ht=scontent.fbud4-1.fna&oh=2c16ee2c3796900cb11ee3188728ca1e&oe=5EDC3E59'},
    {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1',picture: 'https://scontent.fbud4-1.fna.fbcdn.net/v/t1.0-9/79381869_2615681958522716_7282490888616411136_o.jpg?_nc_cat=100&_nc_sid=8024bb&_nc_ohc=-6NOcjtK_eUAX_F7QUR&_nc_ht=scontent.fbud4-1.fna&oh=a3404f072858f1610ddcaa30429591b1&oe=5EDCBB52'},
  ];

  constructor() { }

  ngOnInit(): void {
    this.list = [
      { rows: 2,cols: 2, data: 1},
      { rows: 1,cols: 1, data: 2},
      { rows: 2,cols: 1, data: 3},
      { rows: 1,cols: 1, data: 4},

      { rows: 2,cols: 1, data: 5},
      { rows: 2,cols: 2, data: 6},
      { rows: 1,cols: 1, data: 7},
      { rows: 1,cols: 1, data: 8},

      { rows: 1,cols: 1, data: 10},
      { rows: 2,cols: 1, data: 11},
      { rows: 2,cols: 2, data: 12},
      { rows: 1,cols: 1, data: 13},
    ];
  }
}
