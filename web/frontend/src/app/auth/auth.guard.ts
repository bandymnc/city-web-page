import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { AccountModalComponent } from '../account/account-modal/account-modal.component';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private dialog: MatDialog) { }

  canActivate(next: ActivatedRouteSnapshot, tate: RouterStateSnapshot): boolean {

    if (sessionStorage.getItem('token') != null) {
      return true;
    }
    else {
      this.openDialog();
      return false;
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AccountModalComponent, {
      panelClass: 'myapp-no-padding-dialog',
      maxHeight: window.innerHeight + 'px',
      disableClose: true 
    });

    dialogRef.afterClosed().subscribe(isResultFromLogin => {
      if(isResultFromLogin != null){
        window.location.reload();
      }
    });
  }
}
