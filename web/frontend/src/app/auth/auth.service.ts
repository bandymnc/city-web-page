import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import {AccessAndRefreshToken} from './model/access-and-refresh-token' 

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly JWT_TOKEN = environment.accesTokenName;
  private readonly REFRESH_TOKEN = environment.refreshTokenName;
  token: AccessAndRefreshToken;

  constructor(private http: HttpClient) { }

  refreshToken() {
    this.token = new AccessAndRefreshToken(this.getAccessToken(),this.getRefreshToken());

    return this.http.post<any>(environment.apiEndPoint + 'account/refreshToken', this.token).pipe(tap((res) => {
      this.storeJwtToken(res.token);
    }));
  }

  private getRefreshToken() {
    return localStorage.getItem(environment.refreshTokenName);
  }

   getAccessToken(){
    return localStorage.getItem(environment.accesTokenName);
  }

  private storeJwtToken(jwt: string) {
    return localStorage.setItem(this.JWT_TOKEN, jwt);
  }
}
