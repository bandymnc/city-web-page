export class AccessAndRefreshToken{
    constructor(
        public oldAccessToken: string,
        public refreshToken: string
    ){}
}