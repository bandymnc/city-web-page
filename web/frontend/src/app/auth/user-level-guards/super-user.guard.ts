import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { JwtTokenHandler } from 'src/app/helper/jwt-token-handler';

@Injectable({
  providedIn: 'root'
})
export class SuperUserGuard implements CanActivate {
  constructor(private jwtHandler: JwtTokenHandler) { }

  canActivate(next: ActivatedRouteSnapshot, tate: RouterStateSnapshot): boolean {

    let userLevel = this.jwtHandler.getUserLevel();

    if (userLevel != null && userLevel == "Super User") {
      return true;
    }
    else {
      return false;
    }
  }

}
