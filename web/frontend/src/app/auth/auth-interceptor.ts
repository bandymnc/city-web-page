import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, switchMap, filter, take } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { AuthService } from './auth.service';
import { AccountModalComponent } from '../account/account-modal/account-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private isRefreshing = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(
        private authService: AuthService,
        private dialog: MatDialog,
        private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (localStorage.getItem(environment.accesTokenName) != null) {
            request = this.addToken(request, this.authService.getAccessToken());

            return next.handle(request).pipe(catchError(error => {
                if (error instanceof HttpErrorResponse) {
                    if (error.status === 401) {
                        return this.handle401Error(request, next);
                    } else if (error.status === 404) {
                        this.router.navigate(["**"]);
                    }
                    else if(error.status === 403){
                        localStorage.clear();
                        this.openDialog();
                    }
                }
            }));
        } else {
            return next.handle(request.clone());
        }
    }

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshTokenSubject.next(null);

            return this.authService.refreshToken().pipe(
                switchMap((res: any) => {
                    this.isRefreshing = false;
                    this.refreshTokenSubject.next(res.token);
                    return next.handle(this.addToken(request, res.token));
                }));
        } else {
            return this.refreshTokenSubject.pipe(
                filter(token => token != null),
                take(1),
                switchMap(jwt => {
                    return next.handle(this.addToken(request, jwt));
                }));
        }
    }

    private addToken(request: HttpRequest<any>, token: string) {
        return request.clone({
            setHeaders: {
                'Authorization': `Bearer ${token}`
            }
        });
    }

    openDialog() {
        const dialogRef = this.dialog.open(AccountModalComponent, {
            panelClass: 'myapp-no-padding-dialog',
            maxHeight: window.innerHeight + 'px',
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(isResultFromLogin => {
            if (isResultFromLogin != null) {
                window.location.reload();
            }
        });
    }

}
