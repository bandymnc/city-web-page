export const environment = {
  production: false,
  apiEndPoint: 'https://localhost:44398/api/',

  //Password restrictions
  passwordMinLength: 10,
  passwordMaxLength:20,
  passwordContainUpperCase : true,
  passwordContainDigit: true,

  //JWT
  accesTokenName: "ACCESS_TOKEN",
  refreshTokenName: "REFRESH_TOKEN"

};

