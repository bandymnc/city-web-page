﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Data.Dao
{
    public class Post
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Picture { get; set; }
        public string Title { get; set; }
        public int Views { get; set; }
        public string Body { get; set; }
        public string LastUpdate { get; set; }

        public ApplicationUser User { get; set; }
    }
}
