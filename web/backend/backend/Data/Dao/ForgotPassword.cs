﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Data.Dao
{
    public class ForgotPassword
    {
        public int Id { get; set; }
        public string GeneratedToken { get; set; }
        public DateTime GeneratedTime { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
