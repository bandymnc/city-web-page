﻿using backend.Helper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace backend.Data
{
    public static class ModelBuilderExtensions
    {
        public static void seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityRole>().HasData(
                    new IdentityRole { Name = Roles.Admin, NormalizedName = Roles.Admin.ToUpper() },
                    new IdentityRole { Name = Roles.User, NormalizedName = Roles.User.ToUpper() },
                    new IdentityRole { Name = Roles.SuperUser, NormalizedName = Roles.SuperUser.ToUpper() }
                );
        }

    }
}
