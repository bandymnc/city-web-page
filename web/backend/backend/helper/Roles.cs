﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Helper
{
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string SuperUser = "Super User";
        public const string User = "User";
    }
}
