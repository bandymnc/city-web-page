﻿using backend.DTOs;
using backend.Helper.Exceptions;
using backend.Options;
using backend.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;

namespace backend.Helper
{
    public class FileSystemHelper
    {
        private readonly ApplicationSettings appSettings;

        public FileSystemHelper(IOptions<ApplicationSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
        }

        public FileStructureDto findFoldersAndFiles(string filePath)
        {
            if(!Directory.Exists(filePath))
            {
                filePath =  Path.Combine("Resources", "FileSystem");

                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
            }

            string[] dirs = Directory.GetDirectories(filePath);

            string[] files = Directory.GetFiles(filePath);

            return new FileStructureDto
            {
                Directories = dirs,
                Files = files,
                BasePath = filePath
            };

        }
    
        public bool createNewFolder(string path,string directoryName)
        {
            if (Directory.Exists(path))
            {
                if (!Directory.Exists(path + @"\" + $"{directoryName}"))
                {
                    Directory.CreateDirectory(Path.Combine(path,directoryName));

                    return true;
                }
                throw new UserFriendlyException("Duplicated directory name not allowed","duplicatedDirectoryName");       
            }
             throw new NotFoundException("Directory path don't exists","directoryNotFound");
        }

        public FileDto getFileDecodedInBase64(string filePath)
        {
            if (File.Exists(filePath))
            {
                string[] fileString = filePath.Split("\\");
                string fileName = fileString.Last().Split(".")[0];
                string fileType = fileString.Last().Split(".")[1];

                Byte[] bytes = File.ReadAllBytes(filePath);
                String file = Convert.ToBase64String(bytes);

                return new FileDto
                {
                    File = file,
                    FileName = fileName,
                    FileType = fileType
                };
            }

            throw new FileNotFoundException();
        }

        public bool deleteFile(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);

                return true;
            }

            throw new NotFoundException("File not found", "fileNotFound");
        }

        public bool deleteDirectory(string path, bool recursiveDelete)
        {
            if (Directory.Exists(path))
            {
                if (!recursiveDelete && Directory.GetFiles(path).Length != 0) // nem rekurzív törlés és nem üres a mappa
                {
                    throw new UserFriendlyException("Directory doesn't empty", "allowRecursiveDelete");
                }

                Directory.Delete(path, recursiveDelete);
                return true;
            }

            throw new NotFoundException("Directory not found", "directoryNotFound");
        }

        public bool uploadFiles(UploadFilesViewModel model)
        {
            if (Directory.Exists(model.Path))
            {
                    if (model.File.Length > 0)
                    {
                        var fileName = ContentDispositionHeaderValue.Parse(model.File.ContentDisposition).FileName.Trim('"');
                        var fullPath = Path.Combine(model.Path, fileName);

                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                        model.File.CopyTo(stream);
                        }
                    }

                return true;
            }
            throw new NotFoundException("Directory not found", "directoryNotFound");
        }

        public string uploadImage(IFormFile file)
        {
            var folderName = Path.Combine("Resources", "Images");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

            if (file.Length > 0)
            {
                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                string path = appSettings.Server_URL + dbPath;

                return path;
            }

            throw new UserFriendlyException("Képfeltöltés nem lehetséges", "fileLength");
        }
    }
}



