﻿using System;
namespace backend.Helper.Exceptions
{
    public abstract class BaseException : Exception
    {
        public string Code { get; set; }
        public string ExceptuionMessage { get; set; }

        public BaseException(string msg,string code) : base(msg)
        {
            this.Code = code;
        }

        public BaseException(string msg, string code, string exceptionMessage) : base(msg)
        {
            this.Code = code;
            this.ExceptuionMessage = exceptionMessage;
        }
    }
}
