﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Helper.Exceptions
{
    public class NotFoundException : BaseException
    {
        public NotFoundException(string msg, string code) : base(msg,code)
        {

        }
    }
}
