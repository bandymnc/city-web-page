﻿namespace backend.Helper.Exceptions
{
    public class UserFriendlyException : BaseException
    {
        public UserFriendlyException(string msg,string code) : base(msg,code)
        {
        }

        public UserFriendlyException(string msg, string code,string exceptionMessage) : base(msg, code, exceptionMessage)
        { 
        }
    }
}

