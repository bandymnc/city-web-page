﻿using backend.DTO;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace backend.Helper.Exceptions
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var statusCode = HttpStatusCode.InternalServerError; // 500 if unexpected

            ErrorDto error = new ErrorDto();

            if (ex is UserFriendlyException)
            {
                statusCode = HttpStatusCode.OK;
                
                UserFriendlyException wrongCredentialsException = (UserFriendlyException)ex;

                error.Code = wrongCredentialsException.Code;
                error.Description = wrongCredentialsException.Message;

            }else if(ex is UnauthorizedAccessException)
            {
                statusCode = HttpStatusCode.Forbidden;
                error.Code = "credentials";
                error.Description = "Invalid user or token";

            }else if(ex is NotFoundException)
            {
                NotFoundException notFoundException = (NotFoundException)ex;

                statusCode = HttpStatusCode.NotFound;
                error.Code = notFoundException.Code;
                error.Description = notFoundException.Message;
            }
            else
            {
                error.Code = "server";
                error.Description = ex.Message.ToString();
            }

            var result = JsonConvert.SerializeObject(new { error });
            

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;
            return context.Response.WriteAsync(result);
        }
    }
}
