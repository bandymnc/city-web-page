﻿using backend.Helper.Exceptions;
using backend.Options;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;

namespace backend.Helper.Email
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailSettings emailSettings;

        public EmailSender(IOptions<EmailSettings> emailSettings)
        {
            this.emailSettings = emailSettings.Value;
        }

        public void SendEmail(MessageHelper messageHelper)
        {
            var emailMessage = CreateEmailMessage(messageHelper);

            Send(emailMessage);
        }

        private MimeMessage CreateEmailMessage(MessageHelper messageHelper)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(emailSettings.From));
            emailMessage.To.AddRange(messageHelper.To);
            emailMessage.Subject = messageHelper.Subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Text) { Text = messageHelper.Content };

            return emailMessage;
        }

        private void Send(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    client.Connect(emailSettings.SmtpServer, emailSettings.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(emailSettings.UserName, emailSettings.Password);

                    client.Send(mailMessage);
                }
                catch
                {
                    throw new UserFriendlyException("Email sending not possible","emailSendFail");
                }
                finally
                {
                    client.Disconnect(true);
                    client.Dispose();
                }
            }
        }
    }
}
