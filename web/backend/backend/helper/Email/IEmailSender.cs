﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Helper.Email
{
    public interface IEmailSender
    {
        void SendEmail(MessageHelper messageHelper);
    }
}
