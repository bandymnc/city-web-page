﻿using backend.Data.Dao;
using backend.Options;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace backend.Helper
{
    public class TokenGenerator
    {
        private readonly ApplicationSettings appSettings;

        public TokenGenerator(IOptions<ApplicationSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
        }

        public string generateAccessToken(ApplicationUser user, string userLevel)
        {
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimStore.JwtUserIdClaimName,user.Id.ToString()),
                        new Claim(ClaimTypes.Role,userLevel)
                    }),
                Expires = DateTime.UtcNow.AddMinutes(Double.Parse(appSettings.JWT_Access_Token_Expiration_Time_In_Minute)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.JWT_Secret)), SecurityAlgorithms.HmacSha256)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);

            return token;
        }

        public string generateRefreshToken() 
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
    }
}
