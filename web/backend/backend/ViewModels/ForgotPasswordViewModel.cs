﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [JsonProperty(PropertyName = "generatedToken")]
        public string GeneratedToken { get; set; }

        [JsonProperty(PropertyName = "userId")]
        public string UserId { get; set; }

    }
}
