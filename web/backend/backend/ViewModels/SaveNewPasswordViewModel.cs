﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.ViewModels
{
    public class SaveNewPasswordViewModel
    {
        [JsonProperty(PropertyName = "forgotPasswordToken")]
        public string ForgotPasswordToken { get; set; }

        [JsonProperty(PropertyName = "newPassword")]
        public string NewPassword { get; set; }
    }
}
