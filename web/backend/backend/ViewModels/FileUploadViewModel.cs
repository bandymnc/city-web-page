﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace backend.ViewModels
{
    public class FileUploadViewModel
    {
        [Required]
        public IFormFile file { get; set; }
    }
}
