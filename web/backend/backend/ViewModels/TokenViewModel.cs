﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace backend.ViewModels
{
    public class TokenViewModel
    {
        [Required]
        [JsonProperty(PropertyName = "oldAccessToken")]
        public string OldAccessToken { get; set; }

        [Required]
        [JsonProperty(PropertyName = "refreshToken")]
        public string RefreshToken { get; set; }
    }
}
