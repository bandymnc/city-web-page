﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace backend.ViewModels
{
    public class PaginatonViewModel
    {
        [Required]
        [JsonProperty(PropertyName = "pageSize")]
        public int PageSize { get; set; }

        [Required]
        [JsonProperty(PropertyName = "pageIndex")]
        public int PageIndex { get; set; }

        [JsonProperty(PropertyName = "searchString")]
        public string SearchString { get; set; }

        [Required]
        [JsonProperty(PropertyName = "orderBy")]
        public string OrderBy { get; set; }

        [JsonProperty(PropertyName = "orderByName")]
        public string OrderByName { get; set; }
    }
}
