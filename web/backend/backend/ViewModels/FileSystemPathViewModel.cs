﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.ViewModels
{
    public class FileSystemPathViewModel
    {
        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; }
    }
}
