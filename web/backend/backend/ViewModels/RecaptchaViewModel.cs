﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace backend.ViewModels
{
    public class RecaptchaViewModel
    {
        [Required]
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
    }
}
