﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.ViewModels
{
    public class UserEmailViewModel
    {
        [JsonProperty(PropertyName = "userEmail")]
        public string UserEmail { get; set; }
    }
}
