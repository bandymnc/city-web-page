﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.ViewModels
{
    public class UserRoleUpdateViewModel
    {
        public string UserId { get; set; }
        public string Role { get; set; }
    }
}
