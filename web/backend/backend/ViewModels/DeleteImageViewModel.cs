﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace backend.ViewModels
{
    public class DeleteImageViewModel
    {
        [Required]
        [JsonProperty(PropertyName = "filePath")]
        public string FilePath { get; set; }
    }
}
