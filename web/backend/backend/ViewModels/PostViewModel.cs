﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace backend.ViewModels
{
    public class PostViewModel
    {
        [Required]
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "body")]
        public string Body { get; set; }

        [Required]
        [JsonProperty(PropertyName = "userId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "picture")]
        public string Picture { get; set; }

    }
}
