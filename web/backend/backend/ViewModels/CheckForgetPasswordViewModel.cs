﻿using Newtonsoft.Json;

namespace backend.ViewModels
{
    public class CheckForgetPasswordViewModel
    {
        [JsonProperty(PropertyName = "forgotPasswordGeneratedToken")]
        public string ForgotPasswordGeneratedToken { get; set; }
    }
}
