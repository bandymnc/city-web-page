﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace backend.ViewModels
{
    public class LoginViewModel
    {

        [Required]
        [EmailAddress]
        [JsonProperty(PropertyName ="emal")]
        public string Email { get; set; }

        [Required]
        [StringLength(maximumLength: 20,MinimumLength = 8)]
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }
}
