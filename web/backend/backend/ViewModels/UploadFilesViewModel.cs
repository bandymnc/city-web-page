﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.ViewModels
{
    public class UploadFilesViewModel
    {
        [JsonProperty(PropertyName = "file")]
        public IFormFile File { get; set; }

        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; }

        [JsonProperty(PropertyName = "isProfilePicture")]
        public string IsProfilePicture { get; set; }
    }
}
