﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Data.Dao;
using backend.DTOs;
using backend.Helper;
using backend.Helper.Exceptions;
using backend.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(Roles = Roles.Admin)]
    public class AdministrationController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public AdministrationController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        [Route("users")]
        [HttpPost]
        public async Task<IActionResult> getUsers(PaginatonViewModel model)
        {
            List<UserWithRoleDto> userWithRoleDtos = new List<UserWithRoleDto>();

            // Multiple connection not allowed, ToList() pull the query result into the memory, and allowed another query
            var users = userManager.Users
                .Where(u => (u.FirstName + u.LastName)
                .Contains(model.SearchString))
                .Skip((model.PageIndex) * model.PageSize)
                .Take(model.PageSize)
                .ToList();

            users = customOrderBy(model.OrderByName, model.OrderBy,users);

            foreach (var user in users)
            {
                var role = await userManager.GetRolesAsync(user);

                userWithRoleDtos.Add(new UserWithRoleDto
                {
                    UserId = user.Id,
                    Email = user.Email,
                    Role = role.First(),
                    FullName = user.LastName + " " + user.FirstName
                });
            }

            return Ok(new { users = userWithRoleDtos.Where(u => u.Role != Roles.Admin), count = userManager.Users.Count() });
        }

        [Route("roles")]
        [HttpGet]
        public IActionResult getRoles()
        {
            return Ok(new { roles = roleManager.Roles });
        }

        [Route("updateRole")]
        [HttpPost]
        public async Task<IActionResult> updateUserRole(UserRoleUpdateViewModel model)
        {
            var user = await userManager.FindByIdAsync(model.UserId);

            if (user != null)
            {
                var result = await userManager.RemoveFromRolesAsync(user, await userManager.GetRolesAsync(user));

                if (result.Succeeded)
                {
                    result = await userManager.AddToRoleAsync(user, model.Role);

                    if (result.Succeeded)
                    {
                        return Ok(new { succeeded = true });
                    }
                }

                throw new UserFriendlyException(result.Errors.First().Description, result.Errors.First().Code);
            }

            throw new NotFoundException("User with the given id not found", "userNotFound");
        }

        [Route("deleteUser/{id}")]
        [HttpPost]
        public async Task<IActionResult> deleteUser([FromRoute] string id)
        {
            var user = await userManager.FindByIdAsync(id);

            if (user != null)
            {
                var result = await userManager.DeleteAsync(user);

                if (result.Succeeded)
                {
                    return Ok(new { succeeded = true });
                }
                else
                {
                    throw new UserFriendlyException(result.Errors.First().Code, result.Errors.First().Code);
                }
            }

            throw new NotFoundException("User not found with the given id", "userNotFound");
        }

        private List<ApplicationUser> customOrderBy(string orderByName, string orderBy, List<ApplicationUser> users)
        {
            if (orderBy.Equals("desc"))
            {
                switch (orderByName)
                {
                    case "email":
                       return users.OrderByDescending(u => u.Email).ToList();
                    case "fullName":
                        return users.OrderByDescending(u => u.FirstName + " " + u.LastName).ToList();
                    case "userId":
                        return users.OrderByDescending(u => u.Id).ToList();
                }
            }
            else
            {
                switch (orderByName)
                {
                    case "email":
                        return users.OrderBy(u => u.Email).ToList();
                    case "fullName":
                        return users.OrderBy(u => u.FirstName + " " + u.LastName).ToList();
                    case "userId":
                        return users.OrderBy(u => u.Id).ToList();
                }
            }

            return users;
        }
    }
}