﻿using backend.DTOs;
using backend.Helper;
using backend.Helper.Exceptions;
using backend.Repositories;
using backend.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileSystemController : ControllerBase
    {
        private FileSystemHelper fileSystem;
        private readonly IPostRepository postRepository;

        public FileSystemController(FileSystemHelper fileSystem, IPostRepository postRepository)
        {
            this.fileSystem = fileSystem;
            this.postRepository = postRepository;
        }

        [HttpPost]
        [Route("files")]
        public IActionResult getFiles(FileSystemPathViewModel model)
        {
            FileStructureDto fileStructure = fileSystem.findFoldersAndFiles(model.Path);

            return Ok(new { fileStructure });
        }

        [HttpPost]
        [Route("getFile")]
        public IActionResult getFile(FileSystemPathViewModel model)
        {
            var file = fileSystem.getFileDecodedInBase64(model.Path);

            return Ok(new { file });
        }

        [HttpPost]
        [Route("createNewFolder")]
        public IActionResult newFolder(NewFolderViewModel model)
        {
            if (fileSystem.createNewFolder(model.Path, model.NewFolderName))
            {
                return Ok(new { succeeded = true });
            }

            return BadRequest();
        }

        [HttpPost]
        [Route("deleteFile")]
        public IActionResult deleteFile(FileSystemPathViewModel model)
        {
            if (fileSystem.deleteFile(model.Path))
            {
                return Ok(new { succeeded = true });
            }

            return BadRequest();
        }


        [HttpPost]
        [Route("deleteDirectory")]
        public IActionResult deleteDirectory(FileDeleteViewModel model)
        {
            if (fileSystem.deleteDirectory(model.Path, model.RecursiveDelete))
            {
                return Ok(new { succeeded = true });
            }

            return BadRequest();
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("upload")]
        public IActionResult uploadFiles([FromForm]  UploadFilesViewModel model)
        {
            if (model.IsProfilePicture.Equals("true"))
            {
                string path = fileSystem.uploadImage(model.File);

                if (path.Length != 0)
                {
                    return Ok(new { succeeded = true, path });
                }
            }

            if (fileSystem.uploadFiles(model))
            {
                return Ok(new { succeeded = true });
            }

            throw new NotFoundException("Directory not found", "directoryNotFound");
        }


        [HttpPost]
        [Route("deletePicture")]
        public IActionResult DeleteImg(DeleteImageViewModel model)
        {
            string path = model.FilePath.Substring(model.FilePath.IndexOf("R"));

            if (postRepository.countOfPostsWhichUseThisPicture(path))
            {
                throw new UserFriendlyException("Képtörlés nem lehetséges", "pictureAlreadyUsed");
            }

            if (fileSystem.deleteFile(model.FilePath))
            {
                return Ok(new { Success = true });
            }

            return BadRequest();
        }

    }
}
