﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Data.Dao;
using backend.DTO;
using backend.Helper;
using backend.Helper.Exceptions;
using backend.Repositories;
using backend.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostRepository postRepository;
        private readonly UserManager<ApplicationUser> userManager;

        public PostController(UserManager<ApplicationUser> userManager, IPostRepository postRepository)
        {
            this.postRepository = postRepository;
            this.userManager = userManager;
        }

        [HttpPost]
        [Authorize(Roles = Roles.SuperUser)]
        [Route("new")]
        public async Task<IActionResult> NewPost(PostViewModel postViewModel)
        {
            var user = await userManager.FindByIdAsync(postViewModel.UserId);

            if (user != null)
            {
                Post post = new Post
                {
                    Title = postViewModel.Title,
                    Body = postViewModel.Body,
                    User = user,
                    Picture = postViewModel.Picture,
                    Views = 0,
                    LastUpdate = DateTime.Now.ToString("yyyy.MM.dd HH:mm")
                };

                if (postRepository.add(post))
                {
                    return Ok(new { Success = true });
                }
                else
                {
                    throw new UserFriendlyException("Post mentése sikertelen", "cannotSavePost");
                }
            }

            throw new NotFoundException("Felhasználó nem található", "userNotFound");
        }

        [HttpDelete]
        [Authorize(Roles = Roles.SuperUser)]
        [Route("delete/{id}")]
        public IActionResult deletePost([FromRoute] int id)
        {
            Post post = postRepository.getPostById(id);

            if(post != null)
            {
                postRepository.deletePost(post);

                return Ok(new { succeeded = true });
            }

            throw new NotFoundException("Item with the given id was not found","itemNotFound");
        }

        [HttpPost]
        [Authorize(Roles = Roles.SuperUser)]
        [Route("update/{id}")]
        public async Task<IActionResult> updatePost([FromRoute] int id,PostViewModel model)
        {
            Post post = postRepository.getPostById(id);

            if (post != null)
            {
                post.Body = model.Body;
                post.Picture = model.Picture;
                post.Title = model.Title;
                post.User = await userManager.FindByIdAsync(model.UserId);
                post.LastUpdate = DateTime.Now.ToString("yyyy.MM.dd HH:mm");

                postRepository.updatePost(post);

                return Ok(new { succeeded = true });
            }

            throw new NotFoundException("Item with the given id was not found", "itemNotFound");
        }

        [HttpGet]
        [Route("getAll")]
        public IActionResult GetAll()
        {
            return Ok(postRepository.getAllPost());
        }

        [HttpGet]
        [Route("get/{id}")]
        public IActionResult GetPostById([FromRoute] int id)
        {
            Post post = postRepository.getPostById(id);

            if(post != null)
            {
                post.Views += 1;
                postRepository.updatePost(post);

                return Ok(new { post });
            }

            throw new NotFoundException("Nem található post a megadott id-vel","notFoundPostById");
        }

        [HttpPost]
        [Route("getAllWithPaginator")]
        public ActionResult GetAllWithPaginator(PaginatonViewModel model)
        {
            int length = postRepository.getCountOfPosts();
            List<PostDto> posts = postRepository.getAllPostWithPagination(model);

            return Ok(new { length, posts });
        }
    }
}