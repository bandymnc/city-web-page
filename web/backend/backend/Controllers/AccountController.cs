﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using backend.Data.Dao;
using backend.DTOs;
using backend.Helper;
using backend.Helper.Email;
using backend.Helper.Exceptions;
using backend.Options;
using backend.Repositories;
using backend.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace backend.DTO
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ApplicationSettings appSettings;
        private readonly TokenGenerator tokenGenerator;
        private readonly ITokenRepository tokenRepository;
        private readonly IForgotPasswordRepository forgotPasswordRepository;
        private readonly IEmailSender emailSender;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            IOptions<ApplicationSettings> appSettings,
            TokenGenerator tokenGenerator,
            ITokenRepository tokenRepository,
            IForgotPasswordRepository forgotPasswordRepository,
            IEmailSender emailSender)
        {
            this.userManager = userManager;
            this.appSettings = appSettings.Value;
            this.tokenGenerator = tokenGenerator;
            this.tokenRepository = tokenRepository;
            this.forgotPasswordRepository = forgotPasswordRepository;
            this.emailSender = emailSender;
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            var user = await userManager.FindByEmailAsync(model.Email);

            if (user != null)
            {
                if (await userManager.CheckPasswordAsync(user, model.Password))
                {
                    IList<string> userRoles = await userManager.GetRolesAsync(user);

                    TokenDto tokens = generateTokenResponse(user, userRoles.First());

                    return Ok(new { tokens });
                }
                else
                {
                    throw new UserFriendlyException("Incorrect password", "password");
                }
            }

            throw new UserFriendlyException("Username not found", "userName");
        }

        [HttpPost]
        [Route("Registration")]
        public async Task<IActionResult> Registration(RegistrationViewModel model)
        {
            var user = new ApplicationUser
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                UserName = model.UserName
            };

            IdentityResult result = await userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                result = await this.userManager.AddToRoleAsync(user, Roles.SuperUser);

                //Default Role: User
                if (result.Succeeded)
                {
                    TokenDto tokens = generateTokenResponse(user, Roles.SuperUser);

                    return Ok(new { tokens });
                }
            }

            throw new UserFriendlyException("Registráció sikertelen", "registration");
        }

        [HttpPost]
        [Route("refreshToken")]
        public async Task<IActionResult> GetRefreshToken(TokenViewModel model)
        {
            ClaimsPrincipal prin = GetPrincipalFromExpiredToken(model.OldAccessToken);

            var userId = prin.Claims.Where(c => c.Type.Equals(ClaimStore.JwtUserIdClaimName)).FirstOrDefault().Value;
            var user = await userManager.FindByIdAsync(userId);

            if (user != null)
            {
                Token userRefreshToken = tokenRepository.getTokenByUserId(user);

                if (userRefreshToken != null)
                {
                    //refresh token megegyezik és még nem járt le
                    if (model.RefreshToken.Equals(userRefreshToken.RefreshToken) && DateTime.Now.CompareTo(userRefreshToken.ExpirationDateTime) < 0)
                    {
                        IList<string> userRoles = await userManager.GetRolesAsync(user);
                        string token = tokenGenerator.generateAccessToken(user, userRoles.First());

                        return Ok(new { token });
                    }
                }
            }

            throw new UnauthorizedAccessException("Invalid creditentals");
        }

        [HttpPost]
        [Route("VerifyCaptcha")]
        public async Task<CaptchaVerificationDto> VerifyCaptcha(RecaptchaViewModel recaptcha)
        {
            string userIP = string.Empty;
            var ipAddress = Request.HttpContext.Connection.RemoteIpAddress;

            if (ipAddress != null)
            {
                userIP = ipAddress.MapToIPv4().ToString();
            }

            var payload = string.Format("&secret={0}&remoteip={1}&response={2}", appSettings.Recaptcha_Secret_Key, userIP, recaptcha.Token);

            var client = new HttpClient();
            client.BaseAddress = new Uri("https://www.google.com");
            var request = new HttpRequestMessage(HttpMethod.Post, "/recaptcha/api/siteverify");
            request.Content = new StringContent(payload, Encoding.UTF8, "application/x-www-form-urlencoded");
            var response = await client.SendAsync(request);

            return JsonConvert.DeserializeObject<CaptchaVerificationDto>(response.Content.ReadAsStringAsync().Result);
        }

        [HttpPost]
        [Route("checkForgettenPasswordTokenValidation")]
        public IActionResult CheckForgettenPasswordTokenValidation(CheckForgetPasswordViewModel model)
        {
            ForgotPassword forgotPassword = forgotPasswordRepository.FindTokenByGeneratedToken(model.ForgotPasswordGeneratedToken);

            if (forgotPassword != null)
            {
                DateTime validationDate = forgotPassword.GeneratedTime.AddDays(Double.Parse(appSettings.Change_Password_Token_Life_In_Days));

                if (validationDate > DateTime.Now)
                {
                    return Ok(new { succeeded = true });
                }
                else
                {
                    forgotPasswordRepository.Delete(forgotPassword);

                    throw new UserFriendlyException("The given token not valid.", "tokenNotValid");
                }
            }
            throw new UserFriendlyException("The given token not found.", "tokenNotFound");
        }

        [HttpPost]
        [Route("sendForgotPasswordEmail")]
        public async Task<IActionResult> SendForgotPasswordEmail(UserEmailViewModel model)
        {
            var user = await userManager.FindByEmailAsync(model.UserEmail);

            if (user != null)
            {
                ForgotPassword forgotPassword = forgotPasswordRepository.FindByUserEmail(model.UserEmail);

                if (forgotPassword != null)
                {
                    forgotPassword.GeneratedTime = DateTime.Now;
                    forgotPassword.GeneratedToken = await userManager.GeneratePasswordResetTokenAsync(user);

                    forgotPasswordRepository.UpdateForgotPassword(forgotPassword);
                }
                else
                {
                    forgotPassword = new ForgotPassword
                    {
                        ApplicationUser = user,
                        GeneratedTime = DateTime.Now,
                        GeneratedToken = await userManager.GeneratePasswordResetTokenAsync(user)
                    };

                    forgotPasswordRepository.SaveForgetPasswordToken(forgotPassword);
                }

                //Send Email
                /*var message = new MessageHelper(new string[] { model.UserEmail }, "Elfelejtett jelszó",
                    $"Ha ön kérte a jelszó megújítást, akkor kattintson a következő linkre: " +
                    $"\n http://localhost:4200/changePassword/{forgotPassword.GeneratedToken}");
               
                emailSender.SendEmail(message);*/

                return Ok(new { succeeded = true });
            }

            throw new UserFriendlyException("User not found with the given email", "emailNotFound");
        }

        [HttpPost]
        [Route("saveNewPassword")]
        public async Task<IActionResult> SaveNewPassword(SaveNewPasswordViewModel model)
        {
            var forgotPassword = forgotPasswordRepository.FindTokenByGeneratedToken(model.ForgotPasswordToken);

            if (forgotPassword != null)
            {
                var user = forgotPassword.ApplicationUser;

                if (user != null)
                {
                    var result = await userManager.ResetPasswordAsync(user, model.ForgotPasswordToken, model.NewPassword);

                    if (result.Succeeded)
                    {
                        forgotPasswordRepository.Delete(forgotPassword);

                        return Ok(new { succeeded = true });
                    }

                    throw new UserFriendlyException(result.Errors.First().ToString(), result.Errors.First().Code);
                }
                else
                {
                    throw new NotFoundException("User not found", "userNotFound");
                }
            }

            throw new NotFoundException("The given token not found", "tokenNotFound");
        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            //token validálása, lejárati idő figyelmen kívül hagyása
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.JWT_Secret)),
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }

        private TokenDto generateTokenResponse(ApplicationUser user, string userLevel)
        {
            string refreshToken = tokenGenerator.generateRefreshToken();
            DateTime expirationTime = DateTime.Now.AddHours(Double.Parse(appSettings.JWT_Refresh_Token_Expiration_Time_In_Hour));
            Token userToken = tokenRepository.getTokenByUserId(user);

            //Update
            if (userToken != null)
            {
                userToken.ExpirationDateTime = expirationTime;
                userToken.RefreshToken = refreshToken;

                if (!tokenRepository.updateToken(userToken))
                {
                    throw new UserFriendlyException("Nem lehet frissíteni az access tokent", "accesToken");
                }
            }
            //new
            else
            {
                userToken = new Token
                {
                    RefreshToken = refreshToken,
                    ExpirationDateTime = expirationTime,
                    ApplicationUser = user
                };

                if (!tokenRepository.addToken(userToken))
                {
                    throw new UserFriendlyException("Nem lehet új access tokent menteni", "accesToken");
                }
            }

            return new TokenDto
            {
                AccessToken = tokenGenerator.generateAccessToken(user, userLevel),
                RefreshToken = refreshToken
            };
        }
    }
}