﻿using backend.Data.Dao;
using backend.DTO;
using backend.ViewModels;
using System.Collections.Generic;

namespace backend.Repositories
{
    public interface IPostRepository
    {
        Post getPostById(int id);
        IEnumerable<PostDto> getAllPost();
        bool add(Post post);

        bool deletePost(Post post);

        bool updatePost(Post post);

        List<PostDto> getAllPostWithPagination(PaginatonViewModel model);

        int getCountOfPosts();

        bool countOfPostsWhichUseThisPicture(string pictureName);
    }
}
