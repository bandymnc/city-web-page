﻿using backend.Data;
using backend.Data.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Repositories
{
    public class TokenRepository : ITokenRepository
    {
        private ApplicationDbContext context;
        public TokenRepository(ApplicationDbContext context)
        {
            this.context = context;
        }
        public bool addToken(Token token)
        {
            context.Tokens.Add(token);
            context.SaveChanges();

            return true;
        }

        public Token getTokenByUserId(ApplicationUser user)
        {
          List<Token> tokens = context.Tokens.Where(t => t.ApplicationUser.Equals(user)).ToList();

            if(tokens.Count == 0)
            {
                return null;
            }
          
           Token token1 = tokens.First();
           return context.Tokens.Where(t => t.ApplicationUser.Equals(user)).First();
        }

        public bool updateToken(Token token)
        {
            context.Tokens.Update(token);
            context.SaveChanges();

            return true;
        }
    }
}
