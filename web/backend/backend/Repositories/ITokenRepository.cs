﻿using backend.Data.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Repositories
{
    public interface ITokenRepository
    {
        bool addToken(Token token);

        bool updateToken(Token token);

        Token getTokenByUserId(ApplicationUser user);
    }
}
