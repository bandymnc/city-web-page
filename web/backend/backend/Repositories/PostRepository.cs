﻿using backend.Data;
using backend.Data.Dao;
using backend.DTO;
using backend.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace backend.Repositories
{
    public class PostRepository : IPostRepository
    {
        private ApplicationDbContext context;
        public PostRepository(ApplicationDbContext context)
        {
            this.context = context;
        }
        public bool add(Post post)
        {
            context.Add(post);
            context.SaveChanges();

            return true;
        }

        public bool deletePost(Post post)
        {
            context.Posts.Remove(post);
            context.SaveChanges();

            return true;
        }

        public IEnumerable<PostDto> getAllPost()
        {
            return (from p in context.Posts
                    select new PostDto
                    {
                        LastUpdate = p.LastUpdate,
                        Picture = p.Picture,
                        Title = p.Title,
                        Body = p.Body,
                        UserName = p.User.UserName,
                        Views = p.Views,
                        Id = p.Id
                    }).ToList();
        }

        public List<PostDto> getAllPostWithPagination(PaginatonViewModel model)
        {
            List<PostDto> posts = (from p in context.Posts
                                   where p.LastUpdate.StartsWith(model.SearchString) || p.LastUpdate.EndsWith(model.SearchString)
                                   select new PostDto
                                   {
                                       LastUpdate = p.LastUpdate,
                                       Picture = p.Picture,
                                       Title = p.Title,
                                       Body = p.Body,
                                       UserName = p.User.UserName,
                                       Views = p.Views,
                                       Id = p.Id
                                   })
                    .Skip((model.PageIndex) * model.PageSize)
                    .Take(model.PageSize)
                    .ToList();

            if (model.OrderBy.Equals("dec"))
            {
                return posts.OrderByDescending(o => o.LastUpdate).ToList();
            }

            return posts.OrderBy(o => o.LastUpdate).ToList();
        }

        public int getCountOfPosts()
        {
            return context.Posts.Count();
        }

        public Post getPostById(int id)
        {
            return context.Posts.Find(id);
        }

        public bool countOfPostsWhichUseThisPicture(string pictureName)
        {
            if (context.Posts.Count(p => p.Picture.Contains(pictureName)) > 0)
                return true;

            return false;
        }

        public bool updatePost(Post post)
        {
            context.Posts.Update(post);
            context.SaveChanges();

            return true;
        }
    }
}
