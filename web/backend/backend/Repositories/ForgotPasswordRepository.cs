﻿using backend.Data;
using backend.Data.Dao;
using backend.DTOs;
using backend.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Repositories
{
    public class ForgotPasswordRepository : IForgotPasswordRepository
    {
        private ApplicationDbContext context;

        public ForgotPasswordRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool Delete(ForgotPassword forgotPassword)
        {
            context.ForgotPasswordDaos.Remove(forgotPassword);
            context.SaveChanges();

            return true;
        }

        public bool SaveForgetPasswordToken(ForgotPassword model)
        {
            context.ForgotPasswordDaos.Add(model);
            context.SaveChanges();

            return true;
        }

        public ForgotPassword FindTokenByGeneratedToken(string token)
        {
            var forgotPassword = context.ForgotPasswordDaos.Include(i => i.ApplicationUser).FirstOrDefault(f => f.GeneratedToken.Equals(token));

            if(forgotPassword != null)
            {
                return forgotPassword;
            }

            return null;
        }

        public bool UpdateForgotPassword(ForgotPassword forgotPassword)
        {
            context.ForgotPasswordDaos.Update(forgotPassword);
            context.SaveChanges();

            return true;
        }

        public ForgotPassword FindByUserEmail(string email)
        {
            var forgotPassword = context.ForgotPasswordDaos.FirstOrDefault(f => f.ApplicationUser.Email.Equals(email));

            if(forgotPassword != null)
            {
                return forgotPassword;
            }

            return null;
        }

        public bool DeleteByToken(string token)
        {
            context.ForgotPasswordDaos.Remove(context.ForgotPasswordDaos.First(f => f.GeneratedToken.Equals(token)));

            return true;
        }
    }
}
