﻿using backend.Data.Dao;
using backend.DTOs;
using backend.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Repositories
{
    public interface IForgotPasswordRepository
    {
        bool SaveForgetPasswordToken(ForgotPassword forgotPassword);

        bool UpdateForgotPassword(ForgotPassword forgotPassword);

        ForgotPassword FindTokenByGeneratedToken(string token);

        bool Delete(ForgotPassword forgotPassword);

        bool DeleteByToken(string token);

        ForgotPassword FindByUserEmail(string email);
    }
}
