﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class FileStructureDto
    {
        [JsonProperty(PropertyName = "directories")]
        public string[] Directories { get; set; }

        [JsonProperty(PropertyName = "files")]
        public string[] Files { get; set; }

        [JsonProperty(PropertyName = "basePath")]
        public string BasePath { get; set; }
    }
}
