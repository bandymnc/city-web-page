﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class ForgotPasswordDto
    {
        public string ForgotPasswordToken { get; set; }
        public DateTime GeneratedTime { get; set; }
    }
}
