﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Options
{
    public class ApplicationSettings
    {
        public string JWT_Secret { get; set; }
        public string Client_URL { get; set; }

        public string Recaptcha_Secret_Key { get; set; }
        public string JWT_Access_Token_Expiration_Time_In_Minute { get; set; }
        public string JWT_Refresh_Token_Expiration_Time_In_Hour { get; set; }

        public string Server_URL { get; set; }

        public string Change_Password_Token_Life_In_Days { get; set; }
    }
}
